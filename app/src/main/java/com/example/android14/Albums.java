package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

/**
 * List of Albums Class
 * Allows users to rename, add, delete, search, and open albums
 *
 * @author Ashleigh Chung, Jason Cheng
 */
public class Albums extends AppCompatActivity implements ConfirmFragment.ConfirmFragmentListener {

    static ListView listView;
    public static ArrayList<Album> albums;

    Button addButton, renameButton, openButton, searchButton, deleteButton;
    static ArrayAdapter<Album> adapter;
    public static final int EDIT_ALBUM_CODE = 1;
    public static final int ADD_ALBUM_CODE = 2;
    public static int curr_index = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {//reading
            albums = Database.read();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(albums.size() == 0){
            String[] filenames = new String[]{"ellie", "gary", "ivy", "larry", "shells"};
            albums = new ArrayList<Album>();
            albums.add(new Album("stock"));
            Album stockAlbum = albums.get(0);
            String PATH = "android.resource://" + getPackageName() + "/drawable/";
            Uri uri = null;
            for(int i = 0; i < filenames.length; i++){
                uri = Uri.parse(PATH + filenames[i]);
                String photoPath = uri.getPath();
                String path = uri.getPath();
                String fileName = path.substring(path.lastIndexOf('/') + 1);


                Photo newPhoto = new Photo(photoPath, fileName, uri.toString());
                stockAlbum.getListOfPhotos().add(newPhoto);
            }

        }

        addButton = (Button) findViewById(R.id.button3);
        renameButton = (Button) findViewById(R.id.button);
        openButton = (Button) findViewById(R.id.openAlbum);
        searchButton = (Button) findViewById(R.id.searchButton);
        deleteButton = (Button) findViewById(R.id.button4);

        listView = (ListView) findViewById(R.id.listOfAlbums);
        adapter = new ArrayAdapter<Album>(this, R.layout.album, albums);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((p, V, pos, id) -> curr_index = pos); //-> link to photos not yet implemented

        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ addAlbum();}
        });
        renameButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ showAlbum(curr_index);}
        });
        openButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ openAlbum(curr_index);}
        });
        searchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ search(curr_index);}
        });
        deleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { deleteAlbum(curr_index);}
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter = new ArrayAdapter<Album>(this, R.layout.album, albums);
        listView.setAdapter(adapter);
    }

    //to update edited album list to the database
    @Override
    protected void onPause() {
        super.onPause();
        try {
            Database.write((ArrayList<Album>) albums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //For adding an album
    public void addAlbum(){
        try {
            Intent intent = new Intent(this, AddEditAlbum.class);
            startActivityForResult(intent, ADD_ALBUM_CODE);
        }catch (Exception e){
            Toast.makeText(this, "Select an Album First", Toast.LENGTH_SHORT).show();
        }
    }

    //For editing an album
    public void showAlbum(int pos){
        try {
            Bundle bundle = new Bundle();
            Album album = albums.get(pos);
            if(album.getAlbumName().equals("stock")){
                Toast.makeText(this, "Not Allowed to Edit the Stock Album Name", Toast.LENGTH_SHORT).show();
                return;
            }
            bundle.putInt(AddEditAlbum.ALBUM_INDEX, pos);
            bundle.putString(AddEditAlbum.ALBUM_NAME, album.getAlbumName());
            Intent intent = new Intent(this, AddEditAlbum.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, EDIT_ALBUM_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Select an Album First", Toast.LENGTH_SHORT).show();
        }
    }

    //For opening an ablum
    public void openAlbum(int pos){
        try {
            Album album = albums.get(pos);

            Intent intent = new Intent(this, Photos.class);
            intent.putParcelableArrayListExtra("list", album.getListOfPhotos());
            startActivity(intent);

        } catch (Exception e) {
            Toast.makeText(this, "Select an Album First", Toast.LENGTH_SHORT).show();
        }
    }

    //For search
    public void search(int pos){
        Intent intent = new Intent(this, Search.class);
        intent.putParcelableArrayListExtra("albumsList", albums);
        startActivity(intent);
    }

    //For deleting an album
    public void deleteAlbum(int pos){
        if(curr_index != -1){
            if(albums.get(pos).getAlbumName().equals("stock")){
                Toast.makeText(this, "You Cannot Delete the Stock Album", Toast.LENGTH_SHORT).show();
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString(ConfirmFragment.MESSAGE_KEY,
                    "Are you sure that you want to delete this album?");
            DialogFragment newFragment = new ConfirmFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return;// does not quit activity, just returns from method
        }else{
            Toast.makeText(this, "Select an Album First", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onYesClicked(){
        albums.remove(curr_index);
        listView.setAdapter(
                new ArrayAdapter<Album>(this, R.layout.album, albums));
        curr_index = -1;
    }

    //When user exits pop up screen and returns to main page (add and edit)
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        // gather all info passed back by launched activity
        String name = bundle.getString(AddEditAlbum.ALBUM_NAME);
        int index = bundle.getInt(AddEditAlbum.ALBUM_INDEX);
        if (requestCode == EDIT_ALBUM_CODE) {
            Album album = albums.get(index);
            found:
            {
                for (int i = 0; i < albums.size(); i++) {
                    if (index != i) {
                        Album temp = albums.get(i);
                        if(temp.getAlbumName().equals(name)){
                            Toast.makeText(this, "Album Name Matches Another Album", Toast.LENGTH_SHORT).show();
                            break found;
                        }
                    }
                }
                album.setAlbumName(name);
            }
        } else {
            found:
            {
                for (int i = 0; i < albums.size(); i++) {
                    Album temp = albums.get(i);
                    if(temp.getAlbumName().equals(name)){
                        Toast.makeText(this, "Album Name Matches Another Album", Toast.LENGTH_SHORT).show();
                        break found;
                    }
                }
                albums.add(new Album(name));
            }
        }

        // redo the adapter to reflect change^K
        listView.setAdapter(
                new ArrayAdapter<Album>(this, R.layout.album, albums));
        curr_index = -1;
    }
}


