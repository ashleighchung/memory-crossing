package com.example.android14;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

/**
 * Handles Displaying images for Slideshows
 *
 * @author Jason Cheng
 */
public class ImageAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<Photo> lstSource; //going to be our images

    public ImageAdapter(ArrayList<Photo> lstSource, Context mContext){
        this.lstSource = lstSource;
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return lstSource.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageURI(Uri.parse(lstSource.get(position).getUri()));
        container.addView(imageView, 0);
        return imageView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((ImageView) object);
    }

}