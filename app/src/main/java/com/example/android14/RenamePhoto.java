package com.example.android14;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Rename Photo Class
 *
 * @author Ashleigh Chung, Jason Cheng
 */
public class RenamePhoto extends AppCompatActivity {

    public static final String NEW_CAPTION = "newCaption";
    private String caption;
    private EditText renamePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rename_photo);

        //get the fields
        caption = getIntent().getStringExtra("caption");
        renamePhoto = findViewById(R.id.rename_field);

        if(caption != null){
            renamePhoto.setText(caption);
        }
    }

    public void cancel(View view){
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view){
        //gather all data from text fields
        String name = renamePhoto.getText().toString();

        //pop up dialog if errors in input, and return
        if(name == null || name.trim().length() ==  0){
            return;
        }

        //make Bundle
        Bundle bundle = new Bundle();
        bundle.putString(NEW_CAPTION, name);

        //send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish(); // pops activity from the call stack, returns to parent
    }
}



