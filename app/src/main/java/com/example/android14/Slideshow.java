package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import java.util.ArrayList;

/**
 * Slideshow Class
 * Plays an album
 *
 * @author Jason Cheng
 */
public class Slideshow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);
        ArrayList<Photo> listOfPhotos = getIntent().getParcelableArrayListExtra("list");
        ViewPager viewPager = findViewById(R.id.viewPager);
        ImageAdapter adapter = new ImageAdapter(listOfPhotos, this);
        viewPager.setAdapter(adapter);
    }
}
