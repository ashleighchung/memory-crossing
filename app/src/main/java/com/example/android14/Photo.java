package com.example.android14;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Photo Object Class
 *
 * @author Ashleigh Chung
 */
public class Photo implements Parcelable, Serializable {

    //Class Attributes
    private String path;
    private Tag locationTag = new Tag(" = ");
    private ArrayList<Tag> listOfPersonTags = new ArrayList<>();;
    private String caption;
    private String uri;

    static final long serialVersionUID  = 1L;

    public Photo(String photoPath, String fileName, String uri){//pass in the photo path when you call the photo constructor
        this.path = photoPath;
        this.caption = fileName;//caption is going to be the filename
        this.uri = uri;
    }

    protected Photo(Parcel in) {
        path = in.readString();
        locationTag = in.readParcelable(Tag.class.getClassLoader());
        listOfPersonTags = in.createTypedArrayList(Tag.CREATOR);
        caption = in.readString();
        uri = in.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public String getUri(){return uri;}

    public String getCaption(){
        if(caption == null){
            return "";
        }
        return caption;
    }

    //editing caption
    public void setCaption(String newCaption){
        this.caption = newCaption;
    }

    //for search purposes
    public ArrayList<Tag> getListOfTags(){
        ArrayList<Tag> list = new ArrayList<Tag>();
        if(!this.locationTag.getTagName().equals(" ") && !locationTag.getTagValue().equals(" "))
            list.add(locationTag);
        if (this.listOfPersonTags != null) {
            for (int i = 0; i < this.listOfPersonTags.size(); i++) {
                if(listOfPersonTags.get(i) != null && !listOfPersonTags.get(i).getTagName().equals(" ") && !listOfPersonTags.get(i).getTagValue().equals(" "))
                    list.add(listOfPersonTags.get(i));
                }
            }
        return list;
    }

    //for photo display purposes
    public Tag getLocationTag(){
        return locationTag;
    }

    //for photo display purposes
    public String printListOfPersonTags() {
        String list = "";
        if (listOfPersonTags != null) {
            for (int i = 0; i < listOfPersonTags.size(); i++) {
                if(i!=0)
                    list = list + ", ";
                list = list + listOfPersonTags.get(i).getTagValue().toLowerCase().trim();
            }
        }
        return list;
    }

    public void addTagToPhoto(Tag newTag){
        if(newTag.getTagName().equals("person")){
            newTag.tagName = newTag.tagName.toLowerCase().trim();
            newTag.tagValue = newTag.tagValue.toLowerCase().trim();
            boolean dontAdd = false;
            for(int i = 0; i < this.listOfPersonTags.size(); i++){
                if(this.listOfPersonTags.get(i).getTagValue().toLowerCase().trim().equals(newTag.tagValue))
                    dontAdd = true;
            }
            if(dontAdd == false)
                this.listOfPersonTags.add(newTag);
        }
        else if(newTag.getTagName().equals("location")){
            newTag.tagName = newTag.tagName.toLowerCase().trim();
            newTag.tagValue = newTag.tagValue.toLowerCase().trim();
            this.locationTag = newTag;
        }
    }

    public void deleteTagFromPhoto(Tag tag){
        tag.tagName = tag.tagName.toLowerCase().trim();
        tag.tagValue = tag.tagValue.toLowerCase().trim();
        if(tag.getTagName().equals("location")){
            locationTag = new Tag(" = ");
        } else if(tag.getTagName().equals("person")){
            for(int i = 0; i < listOfPersonTags.size(); i++){
                if(tag.getTagValue().equals(listOfPersonTags.get(i).getTagValue()))
                    listOfPersonTags.remove(i);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeParcelable(locationTag, flags);
        dest.writeTypedList(listOfPersonTags);
        dest.writeString(caption);
        dest.writeString(uri);
    }
}







