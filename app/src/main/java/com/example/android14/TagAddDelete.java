package com.example.android14;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Tag Add and Delete Class
 * Allows users to add and delete Person or Location Tags on a photo
 *
 * @author Ashleigh Chung
 */
public class TagAddDelete extends AppCompatActivity {

    public static final String TAG_INPUT = "tagInput";
    private EditText tagField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_add_delete);

        //get the fields
        tagField = findViewById(R.id.tagField);
    }

    public void cancel(View view){
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view){
        //gather all data from text fields
        String name = tagField.getText().toString().trim();

        //pop up dialog if errors in input, and return
        if(name == null || name.length() ==  0 || name.equals("")){
            Toast.makeText(this, "Please Enter a Tag", Toast.LENGTH_SHORT).show();
            return;
        }
        if( !name.startsWith("person=") && !name.startsWith("location=") ){
            Toast.makeText(this, "Please Follow Tag Format", Toast.LENGTH_SHORT).show();
            return;
        }
        if(name.startsWith("person=") && name.length() < 8){
            Toast.makeText(this, "Please Attach a Tag Value", Toast.LENGTH_SHORT).show();
            return;
        }
        if(name.startsWith("location=") && name.length() < 10){
            Toast.makeText(this, "Please Attach a Tag Value", Toast.LENGTH_SHORT).show();
            return;
        }

        //make Bundle
        Bundle bundle = new Bundle();
        bundle.putString(TAG_INPUT, name);

        //send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish(); // pops activity from the call stack, returns to parent
    }
}

