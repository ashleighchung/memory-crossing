package com.example.android14;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Tag Object Class
 *
 * @author Ashleigh Chung
 */
public class Tag implements Serializable, Parcelable {

    static final long serialVersionUID  = 1L;

    String fullTag;
    String tagName;
    String tagValue;

    public Tag(String fullTag){
        this.fullTag = fullTag;
        this.tagName = fullTag.substring(0, fullTag.indexOf("=")).toLowerCase();
        this.tagValue = fullTag.substring(fullTag.indexOf("=") + 1);
    }

    protected Tag(Parcel in) {
        fullTag = in.readString();
        tagName = in.readString();
        tagValue = in.readString();
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    public String getTagName(){
        return tagName;
    }

    public String getTagValue(){
        return tagValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullTag);
        dest.writeString(tagName);
        dest.writeString(tagValue);
    }
}