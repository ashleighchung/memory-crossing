package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Handles adding new albums and renaming old ones
 *
 * @author Jason Cheng
 */
public class AddEditAlbum extends AppCompatActivity {
    public static final String ALBUM_INDEX = "albumIndex";
    public static final String ALBUM_NAME = "albumName";

    private int albumIndex;
    private EditText albumName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_album);

        // get the fields
        albumName = findViewById(R.id.album_name);

        // see if info was passed in to populate fields WHEN USER WANTS TO EDIT
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            albumIndex = bundle.getInt(ALBUM_INDEX);
            albumName.setText(bundle.getString(ALBUM_NAME));
        }
    }
    public void cancel(View view){
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view){
        //gather all data from text fields
        String name = albumName.getText().toString().trim();

        //pop up dialog if errors in input, and return
        //name is mandatory

        if(name == null || name.length() ==  0){
            Toast.makeText(this, "Please Enter an Album Name", Toast.LENGTH_SHORT).show();
            return; //does not quit activity, just returns from method
        }
        //make Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX, albumIndex);
        bundle.putString(ALBUM_NAME, name);

        //send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish(); // pops activity from the call stack, returns to parent
    }
}

