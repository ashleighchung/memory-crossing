package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

/**
 * PhotoDisplay Class
 * Displays Photo and gives users the option to rename, add tag, or delete tag to it
 *
 * @author Ashleigh Chung
 */
public class PhotoDisplay extends AppCompatActivity {

    private Photo photo;
    Button renamePhoto, deleteTag, addTag;

    ImageView canvas;
    TextView captionArea;
    TextView personTags;
    TextView locationTags;

    public static final int RENAME_PHOTO = 1;
    public static final int TAG_ADD = 2;
    public static final int TAG_DELETE = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_display);

        //Get Photo from previous screen and get canvas variable
        photo = getIntent().getParcelableExtra("singlePhoto");
        canvas = (ImageView) findViewById(R.id.photo);

        //Place photo in canvas
        canvas.setImageURI(Uri.parse(photo.getUri()));

        //Set Caption in TextView
        captionArea = (TextView) findViewById(R.id.captionArea);
        captionArea.setText(photo.getCaption());

        //Set Tags
        personTags = (TextView) findViewById(R.id.personTags);
        locationTags = (TextView) findViewById(R.id.locationTags);
        personTags.setText(photo.printListOfPersonTags());
        locationTags.setText(photo.getLocationTag().getTagValue());

        //Buttons
        renamePhoto = (Button) findViewById(R.id.renamePhoto);
        deleteTag = (Button) findViewById(R.id.deleteTag);
        addTag = (Button) findViewById(R.id.addTag);

        renamePhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ renamePhoto();}
        });
        deleteTag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ deleteTag();}
        });
        addTag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ addTag();}
        });
    }

    //For editing the caption
    public void renamePhoto(){
        Intent intent = new Intent(this, RenamePhoto.class);
        intent.putExtra("caption", photo.getCaption());
        startActivityForResult(intent, RENAME_PHOTO);
    }

    //For deleting a photo's tag
    public void deleteTag(){
        Intent intent = new Intent(this, TagAddDelete.class);
        startActivityForResult(intent, TAG_DELETE);
    }

    //For adding a photo's tag
    public void addTag(){
        Intent intent = new Intent(this, TagAddDelete.class);
        startActivityForResult(intent, TAG_ADD);
    }

    //to update photo in database
    @Override
    protected void onPause() {
        super.onPause();
        try {
            Album curr_Album = Albums.albums.get(Albums.curr_index);
            curr_Album.listOfPhotos.set(Photos.curr_index, photo);
            Database.write((ArrayList<Album>) Albums.albums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //When we come back from rename, add tag, delete tag
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != RESULT_OK) {
            return;
        }

        if(requestCode == RENAME_PHOTO){
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                return;
            }
            // gather all info passed back by launched activity
            String name = bundle.getString(RenamePhoto.NEW_CAPTION);
            this.photo.setCaption(name);
            captionArea.setText(photo.getCaption());
        }

        else if(requestCode == TAG_ADD){
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                return;
            }
            // gather all info passed back by launched activity
            String name = bundle.getString(TagAddDelete.TAG_INPUT);
            Tag newTag = new Tag(name);
            photo.addTagToPhoto(newTag);
            personTags.setText(photo.printListOfPersonTags());
            locationTags.setText(photo.getLocationTag().getTagValue());
        }

        else if(requestCode == TAG_DELETE){
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                return;
            }
            // gather all info passed back by launched activity
            String name = bundle.getString(TagAddDelete.TAG_INPUT);
            Tag newTag = new Tag(name);
            photo.deleteTagFromPhoto(newTag);
            personTags.setText(photo.printListOfPersonTags());
            locationTags.setText(photo.getLocationTag().getTagValue());
        }
    }
}


