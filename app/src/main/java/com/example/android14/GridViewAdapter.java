package com.example.android14;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * GridView Adapter
 * Handles setting up the gridView display of photos
 *
 * @author Ashleigh Chung, Jason Cheng
 */
public class GridViewAdapter extends BaseAdapter {
    ArrayList<Photo> lstSource;
    Context mContext;

    public GridViewAdapter(ArrayList<Photo> lstSource, Context mContext) {
        this.lstSource = lstSource;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return lstSource.size();
    }

    @Override
    public Object getItem(int position) {
        return lstSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CardView card;
        final ImageView image;
        card = new CardView(mContext);
        //card.setCardBackgroundColor(R.drawable.grid_selection);
        image = new ImageView(mContext);
        card.setLayoutParams(new GridView.LayoutParams(300, 300));
        card.setRadius(5);

        image.setImageURI(Uri.parse(lstSource.get(position).getUri()));
        //image.setImageBitmap(getBitmapFrom(Uri.parse(lstSource.get(position).getUri())));
        card.addView(image);
        return card;
    }

    Bitmap getBitmapFrom(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = mContext.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;
    }
}

