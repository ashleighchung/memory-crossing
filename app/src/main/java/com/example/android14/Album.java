package com.example.android14;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Album Object Class
 * Contains fields/methods for an album
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Album implements Serializable, Parcelable {

    static final long serialVersionUID  = 1L;

    public String albumName;
    public ArrayList<Photo> listOfPhotos;

    public Album(String albumName){
        this.albumName = albumName;
        this.listOfPhotos = new ArrayList<Photo>();
    }

    protected Album(Parcel in) {
        albumName = in.readString();
        listOfPhotos = in.createTypedArrayList(Photo.CREATOR);
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public String getAlbumName(){
        return albumName;
    }

    public void setAlbumName(String newAlbumName){
        albumName = newAlbumName;
    }

    public String toString(){ return albumName;}
    public String getString(){ return albumName;}

    public ArrayList<Photo> getListOfPhotos(){
        return listOfPhotos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(albumName);
        dest.writeTypedList(listOfPhotos);
    }
}



